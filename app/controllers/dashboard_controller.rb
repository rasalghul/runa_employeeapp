class DashboardController < ApplicationController
  def index; end

  def show
    @users = User.search(params[:search])
  end
end