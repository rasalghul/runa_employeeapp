class WorkingDay < ApplicationRecord
  belongs_to :user

  # Checkin must be present in working day
  validates_presence_of :checkin

  # TODO: Refactor lambda
  scope :user_filter, ->(user) { 
    if user.type == 'AdminUser'
      WorkingDay.all.newer_first
    else
      WorkingDay.where(user_id: user.id).newer_first
    end
  }

  # Sort items for working days from newest to oldest
  scope :newer_first, -> { order(created_at: :desc) }

  # Calculate worked minutes or hours for working_day
  def worked_time
    # If check out not nil calculate time else no time to calculate
    if !checkout.nil?
      time = ((checkout - checkin) / 1.hour).round
      if time >= 1
        time.to_s + ' hours.'
      else
        ((checkout - checkin) / 60).round.to_s + ' minutes.'
      end
    else
      '0 time.'
    end
  end
end
