Rails.application.routes.draw do
  namespace :admin do
    resources :users
    resources :admin_users
    resources :working_days

    root to: 'users#index'
  end
  
  resources :working_days
  devise_for :users
  root to: 'dashboard#index'
  get 'users', to: 'dashboard#show'
end
